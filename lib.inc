; По xor-ам vs mov 0 написано вот тут
; https://stackoverflow.com/questions/1135679/does-using-xor-reg-reg-give-advantage-over-mov-reg-0
; Теперь я почти везде поменял на ксоры.

section .text


; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    ; Можно было реализовать через mov и jz, но такой подход требует лишнюю операцию (перемещения)
    cmp byte[rdi+rax], 0
    je .break
    inc rax
    jmp .loop
.break:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi            ; Сохраняем состояние rdi, т.к. после вызова функции там может быть что угодно
    call string_length
    pop rsi             ; Начало строки
    mov rdi, 1          ; Дескриптор на stdout
    mov rdx, rax        ; Длина строки
    mov rax, 1          ; Системная команда write
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi            ; Т.к. мы должны получить ссылку на символ, положим его в стек
    mov rsi, rsp        ; Ссылка на символ
    pop rdi             ; Подчищаем стек
    mov rdi, 1          ; Дескриптор на stdout
    mov rdx, 1          ; Длина одного символа
    mov rax, 1          ; Системная команда write
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    ; Могли испольховать print_char, но так мы сэкономили одну команду mov
    push '\n'           ; Т.к. мы должны получить ссылку на символ, положим его в стек
    mov rsi, rsp        ; Ссылка на символ
    pop rdi             ; Подчищаем стек
    mov rdi, 1          ; Дескриптор на stdout
    mov rdx, 1          ; Длина одного символа
    mov rax, 1          ; Системная команда write
    syscall
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rdi         ; Разбиваемое на цифры число
    mov r9, rsp         ; Сохраняем состояние стека
    mov r10, 10
    push 0
    mov rax, r8         ; Делимое
.loop:
    xor rdx, rdx        ; Очистка от мусора, т.к. div использует rax:rdx для деления
    div r10             ; В rax - результат, в rdx (dl) - остаток от деления
    add dl, '0'         ; Код ASCII
    dec rsp             ; Выделяем место под следующую цифру
    mov [rsp], dl       ; Сохраняем цифру

    test rax, rax
    jne .loop           ; Если число ещё не закончилось, идём на след итерацию

    mov rdi, rsp

    push r9             ; Запоминаем r9 чтобы ничего плохо с ним не случилось
    call print_string
    pop r9
    
    mov rsp, r9         ; Восстанавливаем вершину стека
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .print_unsigned
    neg rdi
    push rdi
    mov rdi, '-'         ; ASCII код для '-' (45)
    call print_char
    pop rdi
.print_unsigned:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
.loop:
    ; Побайтовое сравнение
    mov dl, byte [rdi+rcx]
    cmp dl, byte [rsi+rcx]
    jne .fail

    ; Закончили ли
    test dl, dl
    je .success

    ; i++
    inc rcx
    jmp .loop
.success:
    mov rax, 1
    ret
.fail:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0x0            ; Место под чтение символа
    xor rax, rax        ; Системная команда read
    xor rdi, rdi        ; Дескриптор stdin
    mov rsi, rsp        ; Адрес для чтения на вершину стека
    mov rdx, 1          ; Строки длиной один байт
    syscall
    cmp rax, -1
    pop rax             ; Не изменяет флаги, поэтому сразу достаём значение со стека
    jne .return
    xor rax, rax        ; Обнуляем если файл кончился
.return:
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    dec rsi                 ; Оставляем место для нуль-терминатора
    ; Сохраняем регистры
    push rdi
    push rsi
.trim_loop:
    call read_char

    cmp rax, 0x20
    je .trim_loop
    cmp rax, 0x9
    je .trim_loop
    cmp rax, 0xA
    je .trim_loop

    ; Восстанавливаем регистры
    pop rsi
    pop rdi
    xor rcx, rcx
    
    ; Сохраняем регистры
    push r12
    push r13
    push r14

    mov r12, rdi
    mov r13, rsi
    mov r14, rcx
.loop:
    ; Откопали стоп-нуль
    test rax, rax
    je  .return
    ; Откопали пробел
    cmp rax, 0x20
    je  .return
    cmp rax, 0x9
    je  .return
    cmp rax, 0xA
    je  .return
    
    cmp r14, r13
    jge .fail
    mov [r12+r14], rax
    
    inc r14
    
    call read_char

    jmp .loop
    
    ; Восстанавливаем регистры
.fail:
    xor rax, rax
    jmp .end
.return:
    mov rdx, r14
    mov byte[r12+r14+1], 0x0
    mov rax, r12
.end:
    pop r14
    pop r13
    pop r12
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax                ; Буфер для числа
    xor rdx, rdx                ; Длина числа

.loop:
    mov r8b, [rdi]
    ; Проверяем, закончилось ли число
    cmp r8, '0'
    jl .return
    cmp r8, '9'
    jg .return

    sub r8, '0'

    ; Смещение буфера на один разряд (умножение на 10)
    imul rax, 10

    ; Добавление текущего разряда
    add rax, r8

    ; Следующая итерация
    inc rdx
    inc rdi

    jmp .loop
.return:
    ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r8b, [rdi]
    cmp r8, '+'
    je .positive
    cmp r8, '-'
    je .negative
.unsigned:
    jmp parse_uint
.positive:
    inc rdi
    call parse_uint
    inc rdx
    ret
.negative:
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    
    ; Вырыгиваем, если не помещается
    dec rdx                         ; Не забываем про нуль-терминатор
    cmp rax, rdx
    jg .fail

    xor rcx, rcx

.loop:
    mov r9b, byte[rdi + rcx]
    mov byte[rsi + rcx], r9b
    cmp rcx, rdx
    je .return

    inc rcx
    jmp .loop
.return:
    ret
.fail:
    xor rax, rax
    ret
